function [ambi_out] = ambiPan(s, phi, theta)

    %% AMBIPAN - ambisonics panner of MONO sources for 1st order ambisonics
    %
    % Input parameters:
    %   s - input signal
    %   phi - azimuth
    %   theta - elevation
    %
    % Output parameters:
    %   ambi_out - ambisonics audio in B format
    
    %% FUNCTION IMPLEMENTATION

    % Get normalisation factor for 1st order ambisonics
    norm_coeff_SN3D = normalise(1,0); %normalising factor is equal for all spherical harmonics with order 1
    s = s';
    
    % Compute desired spherical harmonics and output signal
    W = s;
    X = s.*cos(phi).*cos(theta)*norm_coeff_SN3D;
    Y = s.*sin(phi).*cos(theta)*norm_coeff_SN3D;
    Z = s.*sin(theta)*norm_coeff_SN3D;
    
    % Get output signal
    ambi_out = [W X Y Z]';
end

