function [B_form] = ambiConvert(A_form)
%% AMBICONVERT - conversion from ambisonics A format to ambisonics B format 
    %
    % Input parameters:
    %   A_form - input signal in ambisonics A format (1st order)
    %
    % Output parameters:
    %   B_form - output signal in ambisonics B format (1st order)
    
    %% FUNCTION IMPLEMENTATION
    % Define conversion matrix
    conv_mat = [1 1 1 1; 1 1 -1 -1; 1 -1 1 -1; 1 -1 -1 1];
    
    % Get the output signal
    B_form = conv_mat*A_form;
    end
