%% Script for plotting all spherical harmonics to order l (NEED PATH TO FUNCTION NORMALISE AND PLOTTER !!!)
close all; clear; clc;

%% Get data
delta_ph = pi/100; %angle step
az = -pi:delta_ph:pi; %azimuthal angle
alt = -pi/2:delta_ph:pi/2; %altitude (elevation) angle
[phi, theta] = meshgrid(az, alt); %2D grid of parameters (azimuth and altitude)

%% USER INPUT
max_l = 3; %spherical harmonic order (best results for max_l <=3)

%% Computing and plotting the spherical harmonics up to order max_l
% Checking, if the spherical harmonic order is plotable (not too high or
% negative)

    if max_l>3
        fprintf('Prilis velky rad, zkuste zmensit rad (<= 3) ci zobrazit\njen nejake z harmonickych pomoci pluginu\n"Spherical_harmonics_signle"!\n');
        max_l=-1;
    elseif max_l<0
        fprintf('Chybne zadani parametru. Zkuste to znovu.\n');
    else
        ACN = 0;
        figure;
    end

% Calculating the Legendre polynomial and reshaping it for current degree
% and order

for l=0:max_l
    Plm = legendre(l, sin(theta));
    if l==0
        Y = 1;
        m=0;
        ACN = plotter(phi, theta, Y, ACN, l, m, max_l); %plotting the spherical harmonic
    else
        for m=-l:l %spherical harmonic degree
            P = reshape(Plm(abs(m)+1,:,:), size(phi));
            norm = normalise(l,m); %SN3D normalisation
            
            if m<0 
                Y = norm.*P.*sin(abs(m)*phi);
            else
                Y = norm.*P.*cos(abs(m)*phi);
            end
            
            ACN = plotter(phi, theta, Y, ACN, l, m, max_l);
        end
    end 
end
