function [normalised_factor] = normalise(l,m)
%% NORMALISE - compute SN3D normalising factor 
    %
    % Input parameters:
    %  l - order of spherical harmonic
    %  m - degree of spherical harmonic
    %
    % Output parameters:
    %  normalised_factor - computed normalised factor (SN3D normalisation)
    
    %% FUNCTION IMPLEMENTATION

    %simulate Kronecker delta
        if m==0
            delta = 1;
         else
            delta = 0;
         end

     %Compute normalising factor
        num = (2-delta)*factorial(l-abs(m));
        den = 4*pi*factorial(l+abs(m));
        normalised_factor = sqrt(num/den);
end

