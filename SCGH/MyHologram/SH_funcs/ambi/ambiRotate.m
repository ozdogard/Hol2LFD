function [ambi_out] = ambiRotate(s, roll, pitch, yaw)
%% AMBIROTATE - rotate whole ambisonics soundfield 
    %
    % Input parameters:
    %   s - input signal
    %   roll - x-axis rotation angle (deg)
    %   pitch - y-axis rotation angle (deg)
    %   yaw - z-axis rotation angle (deg)
    %  
    %
    % Output parameters:
    %   ambi_out - rotated ambisonics audio in B format
    
    %% FUNCTION IMPLEMENTATION
    % Convert degrees to radians
%     alpha = deg2rad(roll);
%     beta = deg2rad(pitch);
%     gamma = deg2rad(yaw);
    alpha = (roll)/180*pi;
    beta = (pitch)/180*pi;
    gamma = (yaw)/180*pi;
    
    % Definition of rotation matrices
    roll_matrix = [1 0 0 0; 0 1 0 0; 0 0 cos(alpha) -sin(alpha); 0 0 sin(alpha) cos(alpha)];
    pitch_matrix = [1 0 0 0; 0 cos(beta) 0 sin(beta); 0 0 1 0; 0 -sin(beta) 0 cos(beta)];
    yaw_matrix = [1 0 0 0; 0 cos(gamma) -sin(gamma) 0; 0 sin(gamma) cos(gamma) 0; 0 0 0 1];
    
    % Get the output signal
    ambi_out = roll_matrix*pitch_matrix*yaw_matrix*s;
end