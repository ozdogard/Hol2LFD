function [LS_feed] = ambiLS(B_form, phi, theta, N)
%% AMBILS - compute virtual loudspeaker feeds (Max-re)
    %
    % Input parameters:
    %   B_form - input signal in ambisonics B format (1st order)
    %   phi - azimuth of loudspeaker
    %   theta - elevation of loudspeaker
    %   N - number of virtual loudspeakers
    %
    % Output parameters:
    %   LS_feed - signal for selected loudspeaker
    
    %% FUNCTION IMPLEMENTATION
    % Get constants and transform degrees to radians
    n = 1/sqrt(N);
%     az = deg2rad(phi);
%     elev = deg2rad(theta);
    az = (phi)/180*pi;
    elev = (theta)/180*pi;
    
    % Define transform matrix
    D = [n n*(sqrt(3/2)*cos(az)*cos(elev)) n*(sqrt(3/2)*sin(az)*cos(elev)) n*(sqrt(3/2)*sin(elev))];
    
    % Get the output signal
    LS_feed = D*B_form;
