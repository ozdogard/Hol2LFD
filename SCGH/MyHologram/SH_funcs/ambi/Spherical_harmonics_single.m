%% Script for computing spherical harmonics (NEED PATH TO FUNCTION NORMALISE!!!)
close all; clear; clc;

%% Get data
delta_ph = pi/100; %angle step
az = -pi:delta_ph:pi; %azimuth
alt = -pi/2:delta_ph:pi/2; %altitude (elevation)
[phi, theta] = meshgrid(az, alt); %2D grid of parameters (azimuth and altitude)

%% USER INPUT
l = 1; %polynomial order
m = 0; %polynomial degree

%% Calculate Legendre polynomials
% Compute legendre polynomial and select only the desired one (based on prameters l and m)
% MATLAB uses Cordon-Shortley phase [(-1)^m] by definition, but this parameter does
% not have an influence on the shape of spherical harmonic

if l<0
    l=0;
    fprintf('Doslo ke zmene parametru l, nyni je l = %d.\n', l);
end

Plm = legendre(l, sin(theta));

% Check user input (no negative l and m>l)
if l==0 && m==0
    P = 1;
elseif l==0 && m~=0
    m = l;
    fprintf('Doslo ke zmene parametru m, nyni je m = %d.\n', m);
    P = 1;
elseif m>l %case when m>l and l!=0
    m = l;
    fprintf('Doslo ke zmene parametru m, nyni je m = %d.\n', m);
    P = reshape(Plm(abs(m)+1,:,:), size(phi)); %reshape to find desired polynomial
else
    P = reshape(Plm(abs(m)+1,:,:), size(phi));
end

%% Calculate normalisation and desired spherical harmonics

% Get normalization factor (SN3D)
norm = normalise(l,m);

%Compute desired spherical harmonic
if m<0 
    Y = norm.*P.*sin(abs(m)*phi);
else
    Y = norm.*P.*cos(abs(m)*phi);
end

%% Plot spherical harmonic and compute its ACN
ACN = l.^2+l+m; %ACN number
[x,y,z] = sph2cart(phi, theta, abs(Y)); % transform spherical coordinates to carthesian

%Plot desired spherical harmonic
figure
surf(x,y,z);
axis equal;
view(140,20); %default azimuth and altitude
xlabel('x');
ylabel('y');
zlabel('z');
title(sprintf('$Y_{%d}^{%d}$ spherical harmonic (ACN = %d)', l, m, ACN),'interpreter','latex');