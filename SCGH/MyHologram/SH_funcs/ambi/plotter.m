function  ACN_high = plotter(phi, theta, Y, ACN, l, m, max_l)
%% PLOTTER - plotting spherical harmonic functions on one figure 
    %
    % Input parameters:
    %   phi - azimuth (rad)
    %   theta - elevation (rad)
    %   Y - spherical harmonic in spherical coordinates
    %   ACN - ambisonics channel numbering
    %   l - desired order
    %   m - desired degree
    %   max_l - maximal order
    %
    % Output parameters:
    %   ACN_high - input ACN number enlarged by one
    
    %% FUNCTION IMPLEMENTATION
    [x,y,z] = sph2cart(phi, theta, abs(Y));
    subplot(max_l+1, max_l+1, ACN+1);
    surf(x,y,z);
    axis equal;
    view(140,20); %default azimuth and altitude
    xlabel('x');
    ylabel('y');
    zlabel('z');
    title(sprintf('$Y_{%d}^{%d}$ spherical harmonic (ACN = %d)', l, m, ACN),'interpreter','latex');
    
    ACN_high = ACN+1;
end

