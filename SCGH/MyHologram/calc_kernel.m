function K = calc_kernel(lambda, theta_a,phi_a, theta_b, phi_b, R,t_bound, p_bound)
% Kernel Function, incorporate the progpagation and full spherical
% rectification.
% theta_a = observation theta
% phi_a = observation phi
% theta_b = object theta
% phi_b = object theta
% lambda = wavelength
% R = viewing distance


    [Phi_aa,Theta_aa ] = meshgrid(phi_a,theta_a);
    
%     Theta_aa_reshaped = reshape(Theta_aa,1,[]);
%     Phi_aa_reshaped = reshape(Phi_aa,1,[]);
    
    K = zeros(size(theta_b,2),size(phi_b,2),numel(theta_a));
    cos_omega = zeros(size(theta_b,2),size(phi_b,2));
    
    for a= 1: numel(Theta_aa);
        Theta_a = Theta_aa(a);
        Phi_a = Phi_aa(a);
        
%         theta_resampled = pi/2 - linspace(t_bound(1,a), t_bound(2,a), size(theta_b,2));
        theta_resampled = linspace(t_bound(1,a), t_bound(2,a), size(theta_b,2));
        phi_resampled = linspace(p_bound(1,a), p_bound(2,a), size(phi_b,2));

        for i = 1:size(phi_b,2)
            for j = 1:size(theta_b,2)
                Theta_b = theta_b(j);
                Phi_b = phi_b(i);
               % Theta_b = theta_resampled(j);
               %  Phi_b = phi_resampled(i);
                
               % cos omega
                cos_omega(i,j) = sin(Theta_a) .* sin(Theta_b) .* cos(Phi_a - Phi_b) + cos(Theta_a) .* cos(Theta_b);
                acos1 = acos(cos_omega(i,j));

                % some sort of boundary (rect partly covers this) 
                if acos1 > pi/2 |  acos1 <-pi/2
                    K(i,j,a) = 0;
                else
                    K(i,j,a) = rect(acos1/pi) * exp(1i*2*pi*R*cos_omega(i,j)/lambda);
                end
            end
        end
        K(:,:,a) = fftshift(K(:,:,a)); 
    end
    
end



% rect function if x<1/2 => 1
function result = rect(a);
result = zeros(size(a));
result(abs(a)<=1/2)=1;
end