    clear all; close all;

% addpath(genpath('SHtools\'));
addpath(genpath('SH_funcs\'));


%% Settings

% laser wavelength, in meters
lambda = 44*10^-6; %in meters

% size of the object in samples
size_obj = 256;
fft_size = size_obj;
space_multip = 10^-3; % object space (mm)

% viewing distance from the origin 
R = 2.5*10^-3; % in meters

% sampling number
theta_samp = 512;
phi_samp = 1024;

% Spherical Harmonics Order
HOH_order = 3 ;


%% auto-set

% theta and phi sampling array
phi_obj = linspace(0, 2*pi, phi_samp);

% below theta_array can be changed with interp1 idk, if it will change much
if phi_samp == theta_samp
    theta_obj = linspace(0, pi/2, theta_samp);
elseif phi_samp == 2*theta_samp %if we want 2xtheta_samp = phi_samp
    theta_obj = zeros(size(phi_obj));
    theta_obj(1:2:end) = linspace(0, pi/2, theta_samp);
    theta_obj(2:2:end) = linspace(0, pi/2, theta_samp);
else
    theta_obj = linspace(0, pi, theta_samp);
end
% theta_obj = fftshift(theta_obj);
% phi_obj = fftshift(phi_obj);

% spacing of the grid;
spacing = space_multip/size_obj;

% cutoff freq of the 3dFFT
fft_cut = 1/(4*spacing);

% freq bin
freq_bin = fft_cut/(fft_size/2);

% hemispherical surface radius
hemi_r = 1/lambda;

% spatial freqs on the hemispherical surface
u = hemi_r * cos(phi_obj')*sin(theta_obj);
v = hemi_r *sin(phi_obj')* sin(theta_obj);
w = sqrt(hemi_r^2 - u.^2 - v.^2);

% corresponding frequency bins
bin_u = round(u./freq_bin);
bin_v = round(v./freq_bin);
bin_w = round(w./freq_bin);

%% create, transform and index object
[x,y,z,obj] = create_obj(size_obj, space_multip);
% obj = obj.*rand(size(obj)); % add random phase


% obj = createCube();               %different object

% ph0 = pi/2.*rand(size(obj));      % maximum of the phase shift, in radians
% obj = obj.*exp(i*ph0);            % add phase shift


% 3DFFT OBJ, FFT can be further adjusted to increase speed (fftw)
OBJ = fftn(obj, [fft_size fft_size fft_size]);
OBJ = fftshift(OBJ);


hemi_OBJ = zeros(size(OBJ,1),size(OBJ,2));
% sph_OBJ = zeros(size(OBJ));
% % hemispherical components of the object
for i = 1: size(bin_u,1)
   for k = 1:size(bin_u,2)
    hemi_OBJ(i,k) = OBJ(size_obj/2 + bin_u(i,k), size_obj/2 + bin_v(i,k), size_obj/2 + bin_w(i,k)); 
    % sph_OBJ(size_obj/2 + bin_u(i,k), size_obj/2 + bin_v(i,k), size_obj/2 + bin_w(i,k)) =  OBJ(size_obj/2 + bin_u(i,k), size_obj/2 + bin_v(i,k), size_obj/2 + bin_w(i,k)); 
   end
end

hemi_OBJ_phase = angle(hemi_OBJ);
hemi_OBJ_mag = abs(hemi_OBJ);

figure(2)
% Plot the angle (phase) of the FFT result
subplot(2, 2, 1);
imshow(hemi_OBJ_phase, []);
title('Phase of O_s(\Theta,\Phi)')
xlabel('Theta');
ylabel('Phi')

% Plot the magnitude of the FFT result
subplot(2, 2, 2);
imshow(log10(hemi_OBJ_mag), []);
title('Magnitude of O_s(\Theta,\Phi)');
xlabel('Theta');
ylabel('Phi') 

%% Clear Memory
clear u v w bin_u bin_v bin_w hemi_OBJ_phase hemi_OBJ_mag OBJ obj

%% Kernel Function  

% observation point theta,phi pairs this can be the points of the
% lasers        ***********

% 3 and 5
% theta_obs = [0 pi/2 pi/2 pi/2 pi/2 pi]';
% phi_obs = [0 0 pi/2 pi 3*pi/2 0]';

% 3 
% theta_obs = pi/2;
% phi_obs = pi/2;

% 5
% theta_obs = pi/2;
% phi_obs = 3*pi/2;

% theta_obs = [pi/2 pi/2 pi/3 pi/4 pi/6 pi/18];
% phi_obs = [3*pi/2 pi/2 pi/3 pi/4 pi/6 pi/18];

% Middle points of the eight quarter spherical surfaces
% theta_obs = pi - [2*pi/8, 3*pi/8, 4*pi/8, 5*pi/8, 7*pi/8, 9*pi/8, 11*pi/8, 14*pi/8]/2;
% phi_obs = [pi/4, 3*pi/4, 4*pi/4, 5*pi/4, 6*pi/4, 10*pi/4, 12*pi/4, 13*pi/4]/2;

theta_obs = linspace(0, pi, (HOH_order+1)^2);
phi_obs = linspace(0, 2*pi, (HOH_order+1)^2);
%% prepare for SHT 

%%% Reshaped the observation array 

[Phi_obs,Theta_obs ] = meshgrid( phi_obs,theta_obs);

Theta_obs_reshaped = reshape(Theta_obs,1,[]);
Phi_obs_reshaped = reshape(Phi_obs,1,[]);

%%% the defined for the beginning of Azimuthal and Zenithal Angle are
%%% different for SHT and hemi_OBJ indexing

% theta_phi_arr =[ Phi_obs_reshaped' pi/2-Theta_obs_reshaped'];
% theta_phi_arr =[ Phi_obs_reshaped' pi-Theta_obs_reshaped'];
theta_phi_arr =[ Phi_obs_reshaped' Theta_obs_reshaped'];

boundary_limits = pi/4;

% Calculate the boundaries of the quarters
theta_boundaries = [Theta_obs_reshaped - boundary_limits; Theta_obs_reshaped + boundary_limits];
phi_boundaries = [Phi_obs_reshaped - boundary_limits; Phi_obs_reshaped + boundary_limits];


%% Kernel function nad prepare for sht

% no theta phi boundaries 
% K = calc_kernel(lambda, theta_obs,phi_obs, theta_obj, phi_obj, R, theta_boundaries, phi_boundaries);

% resamples the area between theta and phi boundaries and multiplies with
% and encode using that kernel
K = calc_kernel_1(lambda, theta_obs,phi_obs, theta_obj, phi_obj, R, theta_boundaries, phi_boundaries);

K_reshaped = zeros(phi_samp^2,length(theta_obs));
hemi_OBJ_reshaped = zeros(phi_samp^2,length(theta_obs));

% K_reshaped = zeros(length(theta_obs),phi_samp^2);
% hemi_OBJ_reshaped = zeros(length(theta_obs),phi_samp^2);

for jj = 1:size(K,3)
    K_reshaped(:,jj) = reshape(K(:,:,jj), phi_samp^2,[]);
    hemi_OBJ_reshaped(:,jj) = reshape(hemi_OBJ, phi_samp^2,[]);
end


%% clear memory
 clear K hemi_OBJ

%% Convolution integral
encoded_OBJ = encodeHOH(HOH_order, hemi_OBJ_reshaped, theta_phi_arr);
encoded_K = encodeHOH(HOH_order, K_reshaped, theta_phi_arr);

C = 1i /(4*pi*lambda);
% C = 1/(4*pi*lambda);

fs_sht = C*encoded_K.*encoded_OBJ;
% fs_sht = C * encoded_OBJ; 
% fs_mag_sht = encoded_mag;
% fs_phase_sht = encoded_phase;


%% Decode spherical wavefront from SHT 
method = 'mmd';

theta_out = linspace(0, pi, (HOH_order+1)^2);
% theta_out = pi - linspace(0, pi, (HOH_order+1)^2);
phi_out = linspace(0, 2*pi, (HOH_order+1)^2);
theta_phi_out = [phi_out' theta_out'];

[D, order] = hambiDecoder(theta_phi_out, method, 1, HOH_order);

fs = decodeHOH(fs_sht,D);

% Only K decoding
K_decoded = decodeHOH(encoded_K,D);

f = zeros(2*theta_samp, phi_samp, (HOH_order+1)^2);

%% interference trial
k = 2*pi / lambda;
plane_wave = zeros(size(f,1),size(f,2));
interfered_wave = zeros(size(f));
[Phi_obj, Theta_obj] = meshgrid(phi_obj, theta_obj);



for jj = 1:(HOH_order+1)^2
    f(:,:,jj) = reshape(fs(:,jj),2*theta_samp,[]);

    % Define the plane wave as a complex function based on Theta and Phi
    plane_wave(:,:) = exp(1i * k * sin(Theta_obj) .* cos(Phi_obj));

    % Interfere the wavefront with the plane wave
    interfered_wave(:,:,jj) = (f(:,:,jj) + plane_wave(:,:)).^2;

end



%% Spherical wavefront channels
% hemispherical components of the object
figure()
% Plot the magnitude of the FFT result
for  source_no = 1:jj

%     % try to put the wavefront back on the 1/lambda sphere that it was picked from
%   
%     recons_obj = zeros(size(OBJ,1),size(OBJ,2),size(OBJ,3));
%     
%     for i = 1: size(bin_u,1)
%        for k = 1:size(bin_u,2)
%             recons_obj(size_obj/2 + bin_u(i,k), size_obj/2 + bin_v(i,k), size_obj/2 + bin_w(i,k)) = f(i,k,source_no); 
%        end
%     end
    pause(0.5)
        subplot(1, 2, 2);
        imshow(log10(abs(f(:,:,source_no).^2)), []);
        title('Magnitude');
        
        subplot(1, 2, 1);
        imshow(angle(f(:,:,source_no).^2), []);
        title('K*OBJ phase');
    
    pause(0.5)
end


%%% Searching for meaning
% figure()
% % Plot the magnitude of the FFT result
% for  source_no = 1:jj
%     % try to put the wavefront back on the 1/lambda sphere that it was picked from
% %     recons_obj = zeros(size(OBJ,1),size(OBJ,2),size(OBJ,3));
% %     
% %     for i = 1: size(bin_u,1)
% %        for k = 1:size(bin_u,2)
% %             recons_obj(size_obj/2 + bin_u(i,k), size_obj/2 + bin_v(i,k), size_obj/2 + bin_w(i,k)) = f(i,k,source_no); 
% %        end
% %     end
% 
% 
%    pause(0.5)
% 
%     subplot(1, 2, 2);
% imshow(log10(abs(ifft(ifftshift(f(:,:,source_no))).^2)), []);
%  title('K*OBJ ifft SQUARED');
% % Plot the angle (phase) of the FFT result
% subplot(1, 2, 1);
% imshow(angle(f(:,:,source_no).^2), []);
% pause(0.5)
% end


%% INTERFERED

figure()

for  source_no = 1:jj
    pause(0.5)

    subplot(1, 2, 2);
imshow(log10(abs(interfered_wave(:,:,source_no))), []);
title('K*OBJ');

% Plot the angle (phase) of the FFT result
subplot(1, 2, 1);
imshow(angle(interfered_wave(:,:,source_no)), []);
title('interference + K*OBJ');
pause(0.5)
end

%% ONLY ENCODED OBJ 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C = 1;
fs_sht = C * encoded_OBJ; 
% fs_mag_sht = encoded_mag;
% fs_phase_sht = encoded_phase;


method = 'mmd';
[D, order] = hambiDecoder(theta_phi_out, method, 1, HOH_order);
fs = decodeHOH(fs_sht,D);
f = zeros(2*theta_samp, phi_samp, (HOH_order+1)^2);
for jj = 1:(HOH_order+1)^2
    f(:,:,jj) = reshape(fs(:,jj),2*theta_samp,[]);
end


figure(2)
subplot(2, 2, 4);
imshow(log10(abs(f(:,:,1))), []);
title('Magnitude of O_s Decoded');
xlabel('Theta')
ylabel('Phi')

for  source_no = 1:jj
% Plot the angle (phase) of the FFT result
subplot(2, 2, 3);
imshow(angle(f(:,:,source_no)), []);
title('Phase of O_s Decoded')
pause(0.5)
end
xlabel('Theta')
ylabel('Phi')

% maybe add rotation but that depends on the success of the implementation































%%
















%% Take 3D FFT  at a distance R but include WRP METHOD 

% WRP https://opg.optica.org/oe/fulltext.cfm?uri=oe-23-17-22149&id=324082


% 
% In the following explanation, steps 1 to 3 are preparation steps, step 4 handles the intra- and inter-WRP propagation and is processed for every WRP, and step 5 is calculating the diffraction field at the hologram plane by propagating WRPL.
% Pre-processing steps
% 
%     1. The optimal number of WRPs L and the size of the occlusion mask Wocc (optional) are calculated based on the depth of the object and the lateral density of the point light sources of the object, respectively.
%     2. The depth of the point cloud is subdivided uniformly in L WRP zones and each WRP zone is further quantized in depth levels, half of them in front of each WRP and half of them behind it. The points are grouped per WRP zone.
%     3. The optical field of a point for each depth level in a WRP zone is precalculated by backward or forward propagation, creating a table with the fields for each depth level. Each level has a different support window 2Wj, as shown in Eq. (2). This LUT is the same for all WRP zones and its size depends on the depth resolution and the size of the WRP zone.
% 
% Intra- and inter-WRP propagation
% 
%     4. The WRP zones are processed sequentially starting at WRP1 and ending at WRPL, where for every WRP zone, the processing order of the points is by decreasing z-coordinates to facilitate the occlusion processing.
% 
%     Per point we:
%         determine the depth level;
%         apply the occlusion mask in the area around the point on the current WRP to suppress the diffraction pattern of the occluded points (except for the first point);
%         add the optical field contribution from the LUT for the given depth level to the current WRP.
% 
%     When all the points within one WRP zone are calculated, the current WRP diffraction pattern is propagated by angular spectrum propagation to the next WRP. As explained in section 3.1, the kernel of the inter-WRP propagation is precalculated, since it is the same for all the inter-WRP propagations. This step is then repeated for the next WRP until WRPL is reached.
% 
% CGH Generation
% 
%     5. When the last WRP zone (L) is calculated, the diffraction pattern is propagated to the hologram plane (HP) by angular spectrum propagation.


% output diffraction on spherical surface at distance R including occlusion
% culling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% GET DECODING MATRIX BASED ON THE T-DESIGN LOCATION


%% DECODE


%% RECONSTRUCT



