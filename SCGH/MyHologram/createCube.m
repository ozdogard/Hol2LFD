function cube = createCube()
    
    size_im = 256;
    space_multip = 10^-3;
    size_obj = size_im;
    [X, Y, Z] = meshgrid(linspace(0*space_multip, 1*space_multip, size_obj));

    % Load the built-in TIFF images
    image1 = imread('tire.tif');
    image2 = imread('moon.tif');
    image3 = imread('cameraman.tif');
    image4 = imread('autumn.tif');
    image5 = imread('board.tif');
    image6 = imread('eight.tif');
% Convert images to grayscale (black and white)
image1 = im2gray(image1);
image2 = im2gray(image2);
% image3 = im2gray(image3);
image4 = im2gray(image4);
image5 = im2gray(image5);
image6 = im2gray(image6);

    % Resize images to 256x256
image1 = imresize(image1, [size_im, size_im]);
image2 = imresize(image2, [size_im, size_im]);
image3 = imresize(image3, [size_im, size_im]);
image4 = imresize(image4, [size_im, size_im]);
image5 = imresize(image5, [size_im, size_im]);
image6 = imresize(image6, [size_im, size_im]);
    % Create the cube
    cube = zeros(256, 256, 256);

    % Assign images to the cube faces
    cube(1:size_im, 1:size_im, 1) = image1;    % Face 1 (Front)
    cube(1:size_im, 1:size_im, size_im) = image2;  % Face 2 (Back)
    cube(1:size_im, 1, 1:size_im) = image3;    % Face 3 (Top)
    cube(1:size_im, size_im, 1:size_im) = image4;  % Face 4 (Bottom)
    cube(1, 1:size_im, 1:size_im) = image5;    % Face 5 (Left)
    cube(size_im, 1:size_im, 1:size_im) = image6;  % Face 6 (Right)

    
    % Create a 256x256x256 matrix filled with zeros
matrix = zeros(256, 256, 256);

% Define the radius of the sphere
radius = 64;

% Define the center of the sphere
center = [128, 128, 128];

% Create a grid of coordinates using meshgrid
[x, y, z] = meshgrid(1:256, 1:256, 1:256);

% Calculate the distance from each point to the center
distances = sqrt((x - center(1)).^2 + (y - center(2)).^2 + (z - center(3)).^2);

% Set the values within the sphere to 1
matrix(distances <= radius) = 1;
cube = matrix.*rand(size(matrix));
% % Plot the shifted 3D object
figure;
% % patch(isosurface(X, Y, Z, cube));
% % daspect([1, 1, 1]);  % Set equal aspect ratios
% % view(3);  % Set the 3D view
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% title('Shifted 3D Object');
% 
volshow(cube);


end