function [X,Y,Z,obj]= create_obj(size_obj,space_multip)

% % Define the size of the cube
% size_obj = 256;
% space_multip = 10^-3

% Create the cube
[X, Y, Z] = meshgrid(linspace(-1*space_multip, 1*space_multip, size_obj));

obj = (abs(X) <= 1) & (abs(Y) <= 1) & (abs(Z) <= 1);

% Create circular holes on each surface
radius = size_obj / 4;
middle_point = [size_obj / 2, size_obj / 2, size_obj / 2];  % Middle point of the cube

for i = 1:size_obj
    for j = 1:size_obj
        if sqrt((i - middle_point(1))^2 + (j - middle_point(2))^2) <= radius
            obj(i, j, :) = false;
            obj(:, i, j) = false;
            obj(i, :, j) = false;
        end
    end
end

% Plot the shifted 3D object
figure;
% volshow(obj)
patch(isosurface(X, Y, Z, obj), 'FaceColor', 'red', 'EdgeColor', 'green');
daspect([1, 1, 1]);  % Set equal aspect ratios
view(3);  % Set the 3D view
xlabel('X');
ylabel('Y');
zlabel('Z');
title('Initial Object in Cartesian Coordinates');

end