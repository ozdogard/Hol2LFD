
clear all;
close all;

x = linspace(-10,20,300);
y1 = sin(x);
y2 = sin(x)+1;
y3 = sin(x)+2;
y4 = sin(x)+3;
y5 = sin(x)+4;
figure('Color','w')
hold on

text(-9,4.5,'Wave 1','Color','black','FontSize',14)
plot(x,y1,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,3.5,'Wave 2','Color','black','FontSize',14)
plot(x,y2,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,2.5,'Wave 3','Color','black','FontSize',14)
plot(x,y3,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,1.5,'Wave 4','Color','black','FontSize',14)
plot(x,y4,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,0.5,'Wave 5','Color','black','FontSize',14)
plot(x,y5,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

plot(x,y5+1,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])
plot(x,y5+2,'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

% Find the maximum amplitude and the corresponding x-coordinate
[y,xi] = findpeaks(y1);

% Plot the line connecting the highest sine wave to the lowest sine wave
xline(x(xi),'Color','k','LineWidth',2)

set(gca,'visible','off')
    



clear all;
close all;

x = linspace(-10,20,300);
y1 = sin(x);
y2 = sin(x)+1;
y3 = sin(x)+2;
y4 = sin(x)+3;
y5 = sin(x)+4;
figure('Color','w')
hold on

text(-9,4.5,5,'Wave 1','Color','black','FontSize',14)
plot3(x,y1,5*ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,3.5,4,'Wave 2','Color','black','FontSize',14)
plot3(x,y2,4*ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,2.5,3,'Wave 3','Color','black','FontSize',14)
plot3(x,y3,3*ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,1.5,2,'Wave 4','Color','black','FontSize',14)
plot3(x,y4,2*ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

text(-9,0.5,1,'Wave 5','Color','black','FontSize',14)
plot3(x,y5,ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

plot3(x,y5+1,0.5*ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])
plot3(x,y5+2,0.2*ones(size(x)),'--','LineWidth',1,'Color',[0, 0, 0, 0.5])

% Find the maximum amplitude and the corresponding x-coordinate
[y,xi] = findpeaks(y1);

% Plot the line connecting the highest sine wave to the lowest sine wave

set(gca,'visible','off')
view(-45,30)
