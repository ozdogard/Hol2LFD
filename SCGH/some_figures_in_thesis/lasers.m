N = 121; % number of points
indices = 0:N-1;
y = 1 - (indices ./ (N-1)) .* 2; % y goes from 1 to -1
radius = sqrt(1 - y.^2); % radius at y

goldenRatio = pi * (3 - sqrt(5)); % golden angle in radians
theta = goldenRatio * indices; % golden angle increment

x = cos(theta) .* radius;
z = sin(theta) .* radius;

% Convert RGB color of turquoise to range 0-1
turquoise = [48, 213, 200] ./ 255;

% Add two circles in the middle, perpendicular to each other
hold on;
thetaCircle = linspace(0,2*pi,100);
xCircle = cos(thetaCircle);
yCircle = sin(thetaCircle);
zCircle = zeros(size(thetaCircle));

% Create the subplot
figure('Color','k'); % dark gray background

% Subplot for the sphere
subplot(1, 2, 1);
scatter3(x, y, z, 20, 'MarkerEdgeColor',turquoise,'MarkerFaceColor',turquoise);
axis equal;
grid off;
axis off;
hold on;
% Circle in the x-y plane
plot3(xCircle, yCircle, zCircle, 'w', 'LineWidth', 2);
% Circle in the x-z plane
plot3(xCircle, zCircle, yCircle, 'w', 'LineWidth', 2);
hold off;

N = 242; % number of points
indices = 0:N-1;
y = 1 - (indices ./ (N-1)) .* 2; % y goes from 1 to -1
radius = sqrt(1 - y.^2); % radius at y

goldenRatio = pi * (3 - sqrt(5)); % golden angle in radians
theta = goldenRatio * indices; % golden angle increment

x = cos(theta) .* radius;
z = sin(theta) .* radius;

% Convert RGB color of turquoise to range 0-1
turquoise = [48, 213, 200] ./ 255;

% Add two circles in the middle, perpendicular to each other
hold on;
thetaCircle = linspace(0,2*pi,100);
xCircle = cos(thetaCircle);
yCircle = sin(thetaCircle);
zCircle = zeros(size(thetaCircle));

% Subplot for the hemisphere
subplot(1, 2, 2);
scatter3(x(y>=0), y(y>=0), z(y>=0), 20, 'MarkerEdgeColor',turquoise,'MarkerFaceColor',turquoise);
axis equal;
grid off;
axis off;
hold on;
% Half circle in the x-y plane
plot3(xCircle, yCircle, zCircle, 'w', 'LineWidth', 2);
% Half circle in the x-z plane
plot3(xCircle, zCircle, yCircle, 'w', 'LineWidth', 2);
hold off;

%%

N = 121; % number of points
indices = 0:N-1;
y = 1 - (indices ./ (N-1)) .* 2; % y goes from 1 to -1
radius = sqrt(1 - y.^2); % radius at y

goldenRatio = pi * (3 - sqrt(5)); % golden angle in radians
theta = goldenRatio * indices; % golden angle increment

x = cos(theta) .* radius;
z = sin(theta) .* radius;

% Convert RGB color of turquoise to range 0-1
turquoise = [48, 213, 200] ./ 255;

% Define sphere resolution
sphereRes = 20;

% Sphere grid
[phi,theta] = meshgrid(linspace(0,pi,sphereRes),linspace(0,2*pi,sphereRes));

% Sphere coordinates
xs = 0.7*sin(phi).*cos(theta);
ys = 0.7*sin(phi).*sin(theta);
zs = 0.7*cos(phi);

% Create the subplot
figure('Color','k'); % dark gray background

% Subplot for the sphere
subplot(1, 2, 1);
scatter3(x, y, z, 20, 'MarkerEdgeColor',turquoise,'MarkerFaceColor',turquoise);
hold on;
% Circle in the x-y plane
plot3(cos(linspace(0,2*pi)), sin(linspace(0,2*pi)), zeros(1,100), 'w', 'LineWidth', 2);
% Circle in the x-z plane
plot3(cos(linspace(0,2*pi)), zeros(1,100), sin(linspace(0,2*pi)), 'w', 'LineWidth', 2);
% Plot inner sphere
surf(xs, ys, zs, 'FaceColor', 'none', 'EdgeColor', 'yellow','EdgeAlpha', 0.5);
hold off;
axis equal;
grid off;
axis off;

N = 242; % number of points
indices = 0:N-1;
y = 1 - (indices ./ (N-1)) .* 2; % y goes from 1 to -1
radius = sqrt(1 - y.^2); % radius at y

goldenRatio = pi * (3 - sqrt(5)); % golden angle in radians
theta = goldenRatio * indices; % golden angle increment

x = cos(theta) .* radius;
z = sin(theta) .* radius;

% Subplot for the hemisphere
subplot(1, 2, 2);
scatter3(x(y>=0), y(y>=0), z(y>=0), 20, 'MarkerEdgeColor',turquoise,'MarkerFaceColor',turquoise);
hold on;
% Half circle in the x-y plane
plot3(cos(linspace(0,2*pi)), sin(linspace(0,2*pi)), zeros(1,100), 'w', 'LineWidth', 2);
% Half circle in the x-z plane
plot3(cos(linspace(0,2*pi)), zeros(1,100), sin(linspace(0,2*pi)), 'w', 'LineWidth', 2);
% Plot inner hemisphere
hold off;
axis equal;
grid off;
axis off;
