% Define the room dimensions
room_width = 10;
room_height = 8;
room_depth = 5;

% Define the saxophone and guitar positions
sax_x = 3;
sax_y = 4;
sax_z = 2;
guitar_x = 7;
guitar_y = 6;
guitar_z = 1;

% Generate a grid of points representing the room
[X,Y,Z] = meshgrid(1:0.1:room_width, 1:0.1:room_height, 1:0.1:room_depth);

% Define the sound pressure produced by the saxophone and guitar
sax_pressure = exp(-0.05*sqrt((X-sax_x).^2 + (Y-sax_y).^2 + (Z-sax_z).^2));
guitar_pressure = exp(-0.05*sqrt((X-guitar_x).^2 + (Y-guitar_y).^2 + (Z-guitar_z).^2));

% Combine the sound pressures to get the total pressure field
total_pressure = sax_pressure + guitar_pressure;

% Visualize the sound field in 3D
figure;
slice(X,Y,Z,total_pressure,[],[],2)
colormap jet
colorbar
xlabel('Width (m)')
ylabel('Height (m)')
zlabel('Depth (m)')
title('3D Visualization of Sound Field')

%%
% Define the room dimensions
room_radius = 10;

% Define the saxophone and guitar positions
sax_r = 3;
sax_theta = pi/4;
sax_phi = pi/3;
guitar_r = 7;
guitar_theta = pi/3;
guitar_phi = pi/2;

% Generate a grid of spherical coordinates
[phi,theta] = meshgrid(linspace(0,2*pi,100),linspace(0,pi,100));
x = room_radius * sin(phi) .* cos(theta);
y = room_radius * sin(phi) .* sin(theta);
z = room_radius * cos(phi);

% Define the sound pressure produced by the saxophone and guitar
sax_pressure = exp(-0.05*sqrt((x-sax_r*sin(sax_phi)*cos(sax_theta)).^2 + (y-sax_r*sin(sax_phi)*sin(sax_theta)).^2 + (z-sax_r*cos(sax_phi)).^2));
guitar_pressure = exp(-0.05*sqrt((x-guitar_r*sin(guitar_phi)*cos(guitar_theta)).^2 + (y-guitar_r*sin(guitar_phi)*sin(guitar_theta)).^2 + (z-guitar_r*cos(guitar_phi)).^2));

% Combine the sound pressures to get the total pressure field
total_pressure = sax_pressure + guitar_pressure;

% Reshape x, y, and z into vectors
x = reshape(x, [], 1);
y = reshape(y, [], 1);
z = reshape(z, [], 1);
total_pressure = reshape(total_pressure, [], 1);

% Visualize the sound field on a sphere
figure;
sphere_radius = room_radius*0.8;
[x_sphere,y_sphere,z_sphere] = sphere(50);
surf(x_sphere*sphere_radius,y_sphere*sphere_radius,z_sphere*sphere_radius,'FaceAlpha',0.3,'EdgeColor','none');
hold on
colormap jet
scatter3(x,y,z,10,total_pressure,'filled');
xlabel('X')
ylabel('Y')
zlabel('Z')
title('Spherical Coordinates Visualization of Sound Field')
colorbar
