% Set up input and output directories
dir1 = 'E:\Note7Backup\Photos\Instagram';
dir2 = 'E:\Note7Backup\Photos\Gallery\owner\Instagrama at';
outDir = 'E:\Pictures';
% Get lists of all files in both directories
files1 = dir(dir1);
files2 = dir(dir2);

% Loop through all files in dir1
for i = 1:length(files1)
    filename = files1(i).name;
    try
        img1 = imread(fullfile(dir1, filename));
    catch
        continue; % Skip this file if there's an error reading it
    end
    
    % Check if the same file exists in dir2
    matchingFile = false;
    for j = 1:length(files2)
        if strcmp(filename, files2(j).name)
            try
                img2 = imread(fullfile(dir2, files2(j).name));
            catch
                matchingFile = true; % Treat this as a match if there's an error reading it
                break;
            end
            matchingFile = true;
            break;
        end
    end
    
    % If the file doesn't exist in dir2, or the images are different, save to outDir
    if ~matchingFile || ~isequal(img1, img2)
        imwrite(img1, fullfile(outDir, filename));
    end
end

% Loop through all files in dir2 that weren't in dir1
for i = 1:length(files2)
    filename = files2(i).name;
    
    % Check if the file also exists in dir1
    matchingFile = false;
    for j = 1:length(files1)
        if strcmp(filename, files1(j).name)
            matchingFile = true;
            break;
        end
    end
    
    % If the file doesn't exist in dir1, try to read it and save to outDir
    if ~matchingFile
        try
            img2 = imread(fullfile(dir2, filename));
        catch
            continue; % Skip this file if there's an error reading it
        end
        imwrite(img2, fullfile(outDir, filename));
    end
end