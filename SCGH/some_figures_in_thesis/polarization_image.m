% Create a figure with four subplots
figure;
subplot(2,2,1);

% Parallel polarization
t = linspace(0, 2*pi, 100);
[X,Y] = meshgrid(t,t);
Z1 = cos(X);
Z2 = cos(X);
surf(X, Y, Z1,'FaceColor','red','FaceAlpha',0.5);
hold on;
surf(X, Y, Z2,'FaceColor','blue','FaceAlpha',0.5);
title('Parallel Polarization');
legend('E_x','E_y');

% Perpendicular polarization
subplot(2,2,2);
Z1 = sin(X);
Z2 = sin(X);
surf(X, Y, Z1,'FaceColor','red','FaceAlpha',0.5);
hold on;
surf(X, Y, Z2,'FaceColor','blue','FaceAlpha',0.5);
title('Perpendicular Polarization');
legend('E_x','E_y');

% Circular polarization
subplot(2,2,3);
Z1 = cos(X);
Z2 = sin(X);
surf(X, Y, Z1,'FaceColor','red','FaceAlpha',0.5);
hold on;
surf(X, Y, Z2,'FaceColor','blue','FaceAlpha',0.5);
title('Circular Polarization');
legend('E_x','E_y');

% Elliptical polarization
subplot(2,2,4);
Z1 = cos(X);
Z2 = 0.7*sin(X);
surf(X, Y, Z1,'FaceColor','red','FaceAlpha',0.5);
hold on;
surf(X, Y, Z2,'FaceColor','blue','FaceAlpha',0.5);
title('Elliptical Polarization');
legend('E_x','E_y');

