function [result, stdout_combined, errmessage] = hol2lfd(real_dir, im_dir, config_dir, hv_pos, aperture_size, rec_distance, output_dir)

result = '';
stdout_combined = '';
errmessage= '';

aperture_size = double(aperture_size);
rec_distance = double(rec_distance);
hv_pos{1} = double(hv_pos{1});
hv_pos{2} = double(hv_pos{2});

% add paths
addpath(genpath('jpeg-pleno-nrsh/'));

try
    [stdout_real, real] = evalc("exrread(real_dir)");
    [stdout_imag, imag] = evalc("exrread(im_dir)");
    
    Hol = real + 1i*imag;

    [stdout_info, info] = evalc("getSettings('cfg_file', config_dir, " + ...
                                                    "'apertureinpxmode', false, " + ...
                                                    "'ap_sizes', aperture_size, " + ...
                                                    "'h_pos', hv_pos{1}, " + ...
                                                    "'v_pos', hv_pos{2}," + ...
                                                    "'resize_fun', @(x) imresize(x, [2048 2048], 'bilinear'))");
    info.outfolderpath = output_dir;

    [stdout_nrsh] = evalc('nrsh(Hol, rec_distance, info)');

catch ME
    error_message = ME.message;
end

% % Combine the stdout strings with related names
% % stdout_combined = ['real: ', stdout_real, '\nimag: ', stdout_imag, '\ninfo: ', stdout_info, '\nnrsh: ', stdout_nrsh];
    if exist('error_message', 'var')
        
        longStr = '';
            for i = 1:numel(ME.stack)
                longStr = [longStr, ME.stack(i).file, ' ', ME.stack(i).name, ' ', num2str(ME.stack(i).line), '\n '];
            end
            stdout_combined = ['\nerror_message: ', error_message ,'\n', longStr];
    end
    stdout = '';

    %         % Display the variables
        tmp = evalc('disp(real_dir);');
        stdout = [stdout, tmp];
        tmp = evalc('disp(im_dir);');
        stdout = [stdout, tmp];
        tmp = evalc('disp(config_dir);');
        stdout = [stdout, tmp];
        tmp = evalc('disp(class(hv_pos{1}));');
        stdout = [stdout, tmp];
        tmp = evalc('disp(length(hv_pos));');
        stdout = [stdout, tmp];
        tmp = evalc('disp(hv_pos{1});');
        stdout = [stdout, tmp];
        tmp = evalc('disp(hv_pos{2});');
        stdout = [stdout, tmp];
        tmp = evalc('disp(aperture_size);');
        stdout = [stdout, tmp];
        tmp = evalc('disp(rec_distance);');
        stdout = [stdout, tmp];
        tmp = evalc('disp(output_dir);');
        stdout = [stdout, tmp, '\n ----- \n'];
    stdout_combined = [stdout_combined,stdout]



end

