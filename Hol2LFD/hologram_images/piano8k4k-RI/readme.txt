This hologram was synthesized using the algorithm developed by the Institute of Research & Technology (IRT) b<>com [1].

This data may be used for research purposes only and may not be commercially distributed. If this data is used in an academic publication, please acknowledge the Advanced Media Coding Lab at IRT b<>com and cite [1].

The parameters used to compute this hologram are listed bellow:
- Resolution: 7680 x 4320
- Pixel pitch: 4.8um
- Red wavelength: 640nm
- Green wavelength: 532nm
- Blue wavelength: 473nm
- Location of the scene: between 0 and 1.8cm

The files 'piano8k4k_real.exr' and 'piano8k4k_imag.exr' correspond to the real and imaginary parts of the object wave within the hologram plane, encoded as floating point numbers using 32bits per channel.

For more information, you can contact Antonin GILLES : antonin.gilles@b-com.com

[1] A. Gilles, P. Gioia, R. Cozot, and L. Morin, �Hybrid approach for fast occlusion processing in computer-generated hologram calculation,� Appl. Opt., AO, vol. 55, no. 20, pp. 5459�5470, Jul. 2016.
