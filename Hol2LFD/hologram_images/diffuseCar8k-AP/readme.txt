This hologram was synthesized using the algorithm developed by the Institute of Research & Technology (IRT) b<>com [1].

This data may be used for research purposes only and may not be commercially distributed. If this data is used in an academic publication, please acknowledge the Advanced Media Coding Lab at IRT b<>com and cite [1].

The parameters used to compute this hologram are listed bellow:
- Resolution: 8192 x 8192
- Pixel pitch: 0.4um
- Red wavelength: 640nm
- Green wavelength: 532nm
- Blue wavelength: 473nm
- Location of the scene: between 0.22 and 0.50cm

The file 'diffuseCar8k_ampli.bmp' corresponds to the object wave amplitude within the hologram plane, and the file 'diffuseCar8k_phase.bmp' corresponds to its phase (between 0 and 2PI), encoded using 8bits per channel.

For more information, you can contact Antonin GILLES : antonin.gilles@b-com.com

[1] A. Gilles, P. Gioia, R. Cozot, and L. Morin, �Computer-generated hologram from Multiview-plus-Depth data considering specular reflections,� in 2016 IEEE International Conference on Multimedia Expo Workshops (ICMEW), 2016, pp. 1�6.
