# library and module
import random
import tkinter as tk
import os, re
from tkinter import END, filedialog
from tkinter import simpledialog
from tkinter.messagebox import askyesno

from PIL import Image, ImageTk
import pynng, cv2
from numpy import asarray
from cbor import cbor
import xlsxwriter
import sys
import matlab.engine
import time
import itertools
import numpy as np

import image_functions as imfuncs

# create the excel file for subjective evaluation
wb = xlsxwriter.Workbook('C:\\Users\\asus\\Desktop\\THESIS\\LFD\\LGP\\LGP\\Image Quality.xlsx')   #modify the path
ws = wb.add_worksheet('my sheet')


class Win:
    # LFD parameters
    __address = 'ipc:///tmp/holoplay-driver.ipc'        # driver url (alternative: "ws://localhost:11222/driver", "ipc:///tmp/holoplay-driver.ipc")
    __socket = pynng.Req0(recv_timeout = 5000, send_timeout = 5000)     # NNG socket
    __dialer = __socket.dial(__address, block = True)       # NNG Dialer of the socket (remove the variable __dialer)


    def __init__(self):
        Image.MAX_IMAGE_PIXELS = None  # this will remove the limitation
        "Initial the application"
        self.root = tk.Tk()
        self.root.title("LIGHT FIELD DISPLAY APPLICATION")
        self.start()
        self.folder = ''
        self.original_stdout = sys.stdout
        self.original_stderr = sys.stderr

        # Inputs
        self.im_loc = tk.StringVar()
        self.im_loc.set('Select Imaginary Part of the Hologram')
        self.real_loc = tk.StringVar()
        self.real_loc.set('Select Real Part of the Hologram')
        self.config_loc = tk.StringVar()
        self.config_loc.set('Select the Configuration File')
#        self.config_loc.trace_add("write", self.name_prefix_func)
        # Angles
        self.min_hangle_var = tk.StringVar()
        self.max_vangle_var = tk.StringVar()
        self.min_vangle_var = tk.StringVar()
        self.max_hangle_var = tk.StringVar()
        self.min_hangle_var.set("Min")
        self.max_vangle_var.set("Max")
        self.min_vangle_var.set("Min")
        self.max_hangle_var.set("Max")
        # Step
        self.hor_step_var = tk.StringVar()
        self.hor_step_var.set('')
        self.ver_step_var = tk.StringVar()
        self.ver_step_var.set('')
        # rec dis
        self.rec_dis_var = tk.StringVar()
        self.rec_dis_var.set('In meters')
        self.aper_size_var = tk.StringVar()
        self.aper_size_var.set('Aperture angle')
        # output
        self.output_name = tk.StringVar()
        self.output_name.set('Write the Output Image File Name (~dir/xxx.png)')
        self.name_prefix = tk.StringVar()
        self.name_prefix.set('Name prefix of the output images')


    # set the input parameters for the hologram processing
    def hol2lfd_input_page(self,frame):
        frame.destroy()
        # some flags to solve calculation errors during page transitions
        self.numerics_calculated = 0
        self.purpose_quilt_demo = 1

        # Frame containing inputs
        self.input_frame = tk.LabelFrame(self.root,text = 'Inputs')
        self.input_frame.pack( fill='both', expand=tk.TRUE)

        def select_directory(var):
            directory = filedialog.askopenfilename()
            var.set(directory)
            if var == self.config_loc:
                print('here')
                self.name_prefix_func()

        def output_directory(var):
            directory = filedialog.askdirectory()
            var.set(directory)


        self.file_box = tk.LabelFrame(self.input_frame, text ='Input Files')
        self.file_box.grid(column=0,row=0, sticky='nsew')
        self.angle_box = tk.LabelFrame(self.input_frame, text ='Select the angles')
        self.angle_box.grid(column=0,row=1, sticky='nsew')
        self.step_box = tk.LabelFrame(self.input_frame, text ='Select number of rows and columns')
        self.step_box.grid(column=0,row=2, sticky='nsew')
        self.rec_box = tk.LabelFrame(self.input_frame, text ='Reconstruction Distance or Distances')
        self.rec_box.grid(column=0,row=3, sticky='nsew')
        self.output_box = tk.LabelFrame(self.input_frame, text ='Choose the output File')
        self.output_box.grid(column=0,row=4, sticky='nsew')
        self.button_box1 = tk.Frame(self.input_frame)
        self.button_box1.grid(column=0,row =5, sticky='nsew')

        # INPUT FILES
        self.real_loc_label = tk.Entry(self.file_box,textvariable = self.real_loc)
        self.real_loc_label.grid(column = 0, row = 0 ,columnspan = 3, sticky='nsew')
        self.file_select_but = tk.Button(self.file_box, text="Select the Real File", command=lambda:select_directory(self.real_loc))
        self.file_select_but.grid(column = 4, row = 0, sticky= 'nsew')

        self.imag_loc_label = tk.Entry(self.file_box,textvariable = self.im_loc)
        self.imag_loc_label.grid(column = 0, row = 1 ,columnspan = 3, sticky='nsew')
        self.file_select_but = tk.Button(self.file_box, text="Select the Imaginary File", command=lambda:select_directory(self.im_loc))
        self.file_select_but.grid(column = 4, row = 1, sticky= 'nsew')

        self.config_loc_label = tk.Entry(self.file_box,textvariable = self.config_loc)
        self.config_loc_label.grid(column = 0, row = 2 ,columnspan = 3, sticky='nsew')
        self.file_select_but = tk.Button(self.file_box, text="Select the Config File", command=lambda:select_directory(self.config_loc))
        self.file_select_but.grid(column = 4, row = 2, sticky= 'nsew')

        # ANGLE INPUTS
        self.vangle_box = tk.Label(self.angle_box, text = 'Vetrical Angle:')
        self.vangle_box.grid(column = 0, row = 0, sticky='nsew')
        self.min_vangle = tk.Entry(self.angle_box, textvariable=self.min_vangle_var)
        self.min_vangle.grid(column = 1,row = 0, sticky='nsew')
        tk.Label(self.angle_box,text = '-').grid(column = 2,row = 0, sticky='nsew')
        self.max_vangle = tk.Entry(self.angle_box, textvariable=self.max_vangle_var)
        self.max_vangle.grid(column = 3, row = 0 , sticky='nsew')

        self.hangle_box = tk.Label(self.angle_box, text = 'Horizontal Angle:')
        self.hangle_box.grid(column = 0, row = 1, sticky='nsew')
        self.min_hangle = tk.Entry(self.angle_box, textvariable=self.min_hangle_var)
        self.min_hangle.grid(column = 1, row = 1, sticky='nsew')
        tk.Label(self.angle_box,text = '-').grid(column = 2,row = 1, sticky='nsew')
        self.max_hangle = tk.Entry(self.angle_box, textvariable=self.max_hangle_var)
        self.max_hangle.grid(column = 3, row = 1, sticky='nsew')

        # STEP INPUTS
        tk.Label(self.step_box, text='No of Vertical Images:').grid(column=0, row=0, sticky='nsew')
        self.vstep = tk.Entry(self.step_box, textvariable=self.ver_step_var)
        self.vstep.grid(column=1, row=0, sticky='nsew')
        tk.Label(self.step_box,text = 'No of Horizontal Images:').grid(column = 2,row = 0, sticky='nsew')
        self.hstep = tk.Entry(self.step_box, textvariable = self.hor_step_var)
        self.hstep.grid(column = 3, row = 0, sticky='nsew')

        # REC DISTANCE
        tk.Label(self.rec_box, text='Reconstruction distance:').grid(column=0, row=0, sticky='nsew')
        self.rec_dis = tk.Entry(self.rec_box, textvariable=self.rec_dis_var)
        self.rec_dis.grid(column=1, row=0, sticky='nsew')
        tk.Label(self.rec_box, text='Aperture Size:').grid(column=2, row=0, sticky='nsew')
        self.aper_size_entry = tk.Entry(self.rec_box, textvariable=self.aper_size_var)
        self.aper_size_entry.grid(column=3, row=0, sticky='nsew')


        # Output FILES
        self.output_name_entry = tk.Entry(self.output_box, textvariable=self.output_name)
        self.output_name_entry.grid(column=0, row=0, columnspan=3, sticky='nsew')
        self.file_output_but = tk.Button(self.output_box, text="Select Folder", command=lambda: output_directory(self.output_name))
        self.file_output_but.grid(column = 4, row = 0, sticky='nsew')
        self.name_prefix_entry = tk.Entry(self.output_box, textvariable=self.name_prefix)
        self.name_prefix_entry.grid(column=0, row=1, columnspan=3, sticky='nsew')

        # BUTTONS
        self.backButton = tk.Button(self.button_box1, text = 'Back', command = lambda: self.home(self.input_frame))
        self.backButton.grid(column = 0, row = 0 , sticky='nsew')

        self.runButton = tk.Button(self.button_box1, text = 'Hol 2 PNG', command = lambda: self.log_page(self.input_frame,0))
        self.runButton.grid(column = 1, row = 0 , sticky='nsew')

        self.logButton = tk.Button(self.button_box1, text='Log Page', command=lambda: self.log_page(self.input_frame,1))
        self.logButton.grid(column=2, row=0, sticky='nsew')

        self.quitButton = tk.Button(self.button_box1, text = 'Quit', command = lambda: self.quit())
        self.quitButton.grid(column = 3, row = 0, sticky="nswe" )

        frames = self.find_frames(self.input_frame)
        self.auto_resizer(self.input_frame,1)
        self.auto_resizer(frames,2)

    #go to home page
    def home(self,frame):
        frame.destroy()
        self.start()

    def parse_rec_config(self, name_prefix):
        def set_func(value):
            if "," in value:
                data = [float(item.strip()) for item in value.split(",")]
            else:
                print(value)
                value = value.strip()
                # try converting to float, otherwise leave as string
                try:
                    value = float(value)
                    if value.is_integer():
                        data = int(value)
                    else:
                        data = float(value)
                except ValueError:
                    data = value.strip()
            return data

        config_dict = {}
        path = f'reconstruction_configs/{name_prefix}_rec.txt'
        if os.path.exists(path):
            with open(path, 'r') as file:
                lines = file.readlines()
                for line in lines:
                    line = line.strip()
                    if not line:  # skip empty lines
                        continue

                    key, value = line.split(":")
                    key = key.strip()

                    if key == "h_pos_min":
                        self.min_hangle = set_func(value)
                        config_dict[key] = self.min_hangle
                    elif key == "h_pos_max":
                        self.max_hangle = set_func(value)
                        config_dict[key] = self.max_hangle
                    elif key == "v_pos_min":
                        self.min_vangle = set_func(value)
                        config_dict[key] = self.min_vangle
                    elif key == "v_pos_max":
                        self.max_vangle = set_func(value)
                        config_dict[key] = self.max_vangle
                    elif key == "vert_im":
                        self.ver_step = set_func(value)
                        config_dict[key] = self.ver_step
                    elif  key == "hor_im":
                        self.hor_step = set_func(value)
                        config_dict[key] = self.hor_step
                    elif key == "ap_sizes":
                        self.aper_size = set_func(value)
                        config_dict[key] = self.aper_size
                    elif key == "rec_distance":
                        self.rec_dis = set_func(value)
                        config_dict[key] = self.rec_dis
                    else:
                        print(f"Config parameter {key} is current unsupported currently, try adding it to nrsh configs")

        else:

            print(f"Reconstruction config: {path} :file not found")

        self.config_dict = config_dict
    def name_prefix_func(self):
        config_name = self.config_loc.get()
        if config_name.endswith('.txt'):
            #print('name_function')
            filename = os.path.basename(config_name)  # Get the filename with extension
            name, ext = os.path.splitext(filename)  # Split the filename and extension
            self.name_prefix.set(name)

    def safe_int(self,input_string,key):
        try:
            if ',' in input_string:
                # Split the string into a list of strings
                strings = input_string.split(",")
                # Convert the list of strings to a list of integers
                numbers = [float(string) for string in strings]
                return self.float_to_int_if_whole(numbers)
            else:
                return self.float_to_int_if_whole(float(input_string))
        except:
            print(self.config_dict)
            if key in self.config_dict.keys():
                return self.config_dict[key]
            else:
                print(f"'{input_string}' is not a valid number, please correct")


    # Gets the string variables and converts them to integers/floats
    # Creates the numerical inputs to other functions
    def numerics(self):
        self.parse_rec_config(self.name_prefix.get())
        # Angles
        self.min_hangle = self.safe_int(self.min_hangle_var.get(),"h_pos_min")
        self.max_hangle = self.safe_int(self.max_hangle_var.get(),"h_pos_max")
        self.min_vangle = self.safe_int(self.min_vangle_var.get(),"v_pos_min")
        self.max_vangle = self.safe_int(self.max_vangle_var.get(),"v_pos_max")
        # Step
        self.hor_step = self.safe_int(self.hor_step_var.get(),"hor_im")
        self.ver_step = self.safe_int(self.ver_step_var.get(),'vert_im')
        ########## Below parameters are in the config file
        ########## Check if the config file exists and if it does set them,
        ########## if anything there is reasonable input it will be changed
        # rec_dis
        self.rec_dis = self.safe_int(self.rec_dis_var.get(),"rec_distance")
        self.aper_size = self.safe_int(self.aper_size_var.get(),"ap_sizes")

        if self.hor_step == 0 or self.ver_step == 0:
            print("Steps cannot be 0, this creates infinite steps")
            time.sleep(2)
            return self.hol2lfd_input_page(self.log_frame)
        else:
            self.hangle = self.round_to_significant_figures(np.linspace(self.min_hangle, self.max_hangle, self.hor_step*self.ver_step),6)
            if not (self.min_vangle == self.max_vangle):
                self.vangle = self.round_to_significant_figures(np.linspace(self.min_vangle, self.max_vangle, self.ver_step),6)
            else:
                self.vangle = [self.min_vangle]
            self.hv_combinations = list(itertools.product(self.hangle, self.vangle))

        self.numerics_calculated = 1 # not to calculate many times unnecessarily

    # NRSH saves whole float as integers 3.0 = 3 etc.
    def float_to_int_if_whole(self,x):
        if isinstance(x, np.ndarray):
            x = x.tolist()
        if isinstance(x, list):
            return [int(i) if isinstance(i, float) and i.is_integer() else i for i in x]
        elif isinstance(x, float) and x.is_integer():
            return int(x)
        else:
            return x
    # NRSH save the images with 6 significant figures, the function ensures np.linspace has 6 sigfigs
    def round_to_significant_figures(self,arr, sigfigs):
        arr = np.array(arr)  # Ensure input is numpy array
        out = np.zeros(arr.shape)  # Initialize output array
        for idx, num in np.ndenumerate(arr):  # Loop over all elements of arr
            if num != 0:  # Skip zero elements
                # Compute power of ten for number
                power = -np.floor(np.log10(np.abs(num))) + (sigfigs - 1)
                # Round number to sigfigs significant figures
                out[idx] = np.round(num * np.power(10, power)) / np.power(10, power)
        return self.float_to_int_if_whole(out)

    #run the matlab function
    def run_hol2lfd(self):
        self.purpose_quilt_demo = 0
        if not self.numerics_calculated:
            self.numerics()
            self.numerics_calculated = 1

        im_loc, im_name = os.path.split(self.im_loc.get())
        real_loc, real_name = os.path.split(self.real_loc.get())
        config_loc, config_name = os.path.split(self.config_loc.get())


        #self.t.join()
        self.eng = matlab.engine.start_matlab()
        # Add the path to MATLAB's search path
        path = 'jpeg-pleno-nrsh/'
        self.eng.eval(f"addpath(genpath('{path}'))", nargout=0)

        for hv in self.hv_combinations:
        # Call the MATLAB function 'hol2lfd' with two inputs
            result = self.eng.hol2lfd(real_loc+'/'+real_name, im_loc+'/'+im_name,config_loc+'/'+config_name,
                                        hv, self.aper_size, self.rec_dis, self.output_name.get(),
                                        nargout=3)


            #result = self.output.result()
            print('Result : ', result)
            print('----------------------------------\n')
            #print('stdout : ', out)
            print('----------------------------------\n')
            #print('stderr : ', err)
            print('----------------------------------\n')

        self.eng.quit()

    # configure all the frames to resize automatically use together with find_frames.
    def auto_resizer(self,frames, weight):
        if type(frames) != list:
            frames = [frames]
        for frame in frames:
            # Get the current grid size
            num_columns, num_rows = frame.grid_size()
            # Configure weights for all rows and columns
            for i in range(num_rows):
                frame.rowconfigure(i, weight=weight)
            for i in range(num_columns):
                frame.columnconfigure(i, weight=weight)

    # find all the child frames of the widget
    def find_frames(self,widget):
        frames = []
        for child in widget.winfo_children():
            # If the child is a frame, add its name to the list
            if isinstance(child, tk.Frame) or isinstance(child, tk.LabelFrame):
                frames.append(child)
        return frames

    # logging page, Problems: Not in Real Time, the current Matlab API doesn't support real time logging
    def log_page(self, frame, forward):
        frame.destroy()

        self.log_frame = tk.LabelFrame(self.root, text = 'Logs')
        self.log_frame.pack(fill='both', expand=tk.TRUE)

        self.log_box = tk.Text(self.log_frame, wrap='word')
        self.log_box.grid(column=0, row=0, columnspan = 4, sticky='nsew')
        self.redirector = TextRedirector(self.log_box)
        self.scrollb = tk.Scrollbar(self.log_frame, command=self.log_box.yview)
        self.scrollb.grid(column=5, row=0, sticky='nsew')
        self.log_box['yscrollcommand'] = self.scrollb.set

        # redirect the System STDOUT and STDERR to TextRedirector class to output it to the Log Text
        sys.stdout = self.redirector
        sys.stderr = self.redirector

        # BUTTONS
        self.homeButton1 = tk.Button(self.log_frame, text = 'Home', command = lambda: self.home(self.log_frame))
        self.homeButton1.grid(column = 0, row = 1 , sticky='nsew')

        self.backtoinputButton = tk.Button(self.log_frame, text = 'Back to Inputs', command = lambda: self.hol2lfd_input_page(self.log_frame))
        self.backtoinputButton.grid(column = 1, row = 1,columnspan = 1, sticky = 'nsew' )

        self.flashButton = tk.Button(self.log_frame, text='LFD page',
                                           command=lambda: self.hol2lfd_send_page(self.log_frame))
        self.flashButton.grid(column=2, row=1, columnspan=1, sticky='nsew')

        self.quitButton = tk.Button(self.log_frame, text = 'Quit', command = lambda: self.quit())
        self.quitButton.grid(column = 3, row = 1, sticky="nswe" )

        frames = self.find_frames(self.log_frame)
        self.auto_resizer(self.log_frame,1)
        self.auto_resizer(frames,2)

        self.root.wait_visibility(self.quitButton)


        if not forward:
            # Append the error message to the Text widget
            self.log_box.config(state=tk.NORMAL)
            self.log_box.insert(tk.END,'\n------------------NEW CALCULATION-------------\n')
            self.log_box.config(state=tk.DISABLED)
            # run the matlab function
            self.run_hol2lfd()


    #quit function
    def quit(self):
        sys.stdout = self.original_stdout
        sys.stderr = self.original_stderr
        self.Exit()

    # Page to Calculate, Display and Send the LFD image
    def hol2lfd_send_page(self,frame):
        sys.stdout = self.original_stdout
        sys.stderr = self.original_stderr

        self.im_addition_made = 0

        if not self.numerics_calculated:
            self.numerics()
            self.numerics_calculated = 1
        frame.destroy()

        height = 600
        width = 800

        # Create an empty image of the right size
        empty_image = Image.new('RGB', (width, height))
        empty_photo = ImageTk.PhotoImage(empty_image)

        def output_directory(var):
            directory = filedialog.askdirectory()
            var.set(directory)

        # create the frames for widgets
        self.send_frame = tk.LabelFrame(self.root, text = 'SEND Inputs')
        self.send_frame.pack(fill='both', expand=tk.TRUE)

        self.send_box = tk.Frame(self.send_frame)
        self.send_box.grid(column = 0 ,row = 0, sticky = 'nsew')

        self.button_box = tk.Frame(self.send_frame)
        self.button_box.grid(column=0,row =1, sticky='nsew')

        if not self.im_addition_made:
            self.output_name.set(self.output_name.get()+'/'+self.name_prefix.get())
            self.im_addition_made = 1

        ## Widgets on the screen
        self.output_location = tk.Entry(self.send_box, textvariable=self.output_name)
        self.output_location.grid(column=0, row=0, columnspan=5, sticky='nsew')
        self.file_output_but = tk.Button(self.send_box, text="Select the Image",
                                         command=lambda: output_directory(self.output_name))
        self.file_output_but.grid(column=5, row=0, sticky='nsew')
        self.name_prefix_entry2 = tk.Entry(self.send_box, textvariable=self.name_prefix)
        self.name_prefix_entry2.grid(column = 0, row = 1, columnspan = 5, sticky = 'nsew')

        self.im_box = tk.Label(self.send_box,image = empty_photo)
        self.im_box.grid(column = 0, row=3, columnspan = 5, sticky = 'nsew')

        self.show_imageBut = tk.Button(self.send_box, text="CALCULATE IMAGE",
                                         command=lambda: self.display_image(width,height, self.output_name.get(), self.im_box))
        self.show_imageBut.grid(column=0, row=2, columnspan= 5, sticky='ew')

        # BUTTONS
        self.homeButton2 = tk.Button(self.button_box, text = 'Home', command = lambda: self.home(self.send_frame))
        self.homeButton2.grid(column = 0, row = 2 , sticky='nsew')

        self.backtoinputButton = tk.Button(self.button_box, text = 'Back to Inputs', command = lambda: self.hol2lfd_input_page(self.send_frame))
        self.backtoinputButton.grid(column = 1, row = 2,columnspan = 2, sticky = 'nsew' )

        self.backtologButton = tk.Button(self.button_box, text = 'Back to Logs', command = lambda: self.log_page(self.send_frame,1))
        self.backtologButton.grid(column =3, row = 2,columnspan = 2, sticky = 'nsew' )

        self.quitButton = tk.Button(self.button_box, text = 'Quit', command = lambda: self.quit())
        self.quitButton.grid(column = 5, row = 2, sticky="nsew" )

        self.sendButton = tk.Button(self.button_box, text='PNG to LFD',
                                           command=lambda: self.send_function())
        self.sendButton.grid(column=0, row=0, columnspan=6,rowspan=2, sticky='nsew')

        frames = self.find_frames(self.send_frame)
        self.auto_resizer(self.send_frame, 1)
        self.auto_resizer(frames, 2)

    # display the image on the hol2lfd_send_page
    # width,height of the diplayed image,
    # im_loc = self.output_name, folder containing images
    # image_label is the label to show the image.
    def display_image(self,width,height,im_loc,image_label):
        if not self.numerics_calculated and not self.purpose_quilt_demo:
            self.numerics()
            self.numerics_calculated = 1

        print('display_image')
        self.ready_to_send = 0
        if not im_loc.endswith('.png'):
            # run the image functions to create the QUILT image
            hol2lfd = imfuncs.Hol2LFD(im_loc, self.name_prefix.get(), self.hangle,self.vangle,self.aper_size, self.rec_dis,self.ver_step,self.hor_step)
            self.images_dict = hol2lfd.check_images() #missing images ideally -1
            if not (self.images_dict == -1):
                lfd_image = hol2lfd.concatenate_images()
                self.img = lfd_image.resize((width, height), Image.LANCZOS)  # Resize image to fit the label
                img = ImageTk.PhotoImage(self.img)
                # Update image_label with the new image
                image_label.config(image=img)
                image_label.image = img  # Keep a reference to the image to prevent it from being garbage collected
                self.ready_to_send = 1
            else:
                lfd_image = None
                # there is no current management if there are missing images.
                # Those parts could be left out black but this would disrupt the hologram
        else:
            print('here')
            # Load an image using PIL's Image module
            img = Image.open(im_loc)
            self.img = img.resize((width, height), Image.LANCZOS)  # Resize image
            if isinstance(self.img,Image.Image):
                print('image')
            else:
                print('not')
            img = ImageTk.PhotoImage(self.img)
            # Update image_label with the new image
            image_label.config(image=img)
            image_label.image = img  # Keep a reference to the image to prevent it from being garbage collected
            self.ready_to_send = 1

    # send the LFD Image to LFD
    def send_function(self):
        if self.ready_to_send:
            numpydata = asarray(self.img)
            numpydata=numpydata.view()[::-1, :, :]
            numpydata=cv2.cvtColor(numpydata, cv2.COLOR_BGR2RGB)
            numpyshape=numpydata.shape
            vx = self.hor_step
            vy = self.ver_step
            #create the correct format for the  image to be send
            if vx and vy != None:
                numpydata={'cmd': {'show': {'targetDisplay': 0, 'source': 'bindata', 'quilt': {'type': 'image', 'settings': {'vx': vx, 'vy': vy, 'vtotal': vx*vy, 'aspect': 0.75, 'invert': False}}}}, 'bin': numpydata}
                print('Loading Image')
                #send the image to the LGP
                cbordump = cbor.dumps(numpydata,image_shape=numpyshape)
                self.__socket.send(cbordump)
        else:
            print('Image not ready to send')
            pass


    ########################   PREVIOUS CODE FIXED BY ARDA ##############################################
    def start(self):
        "Contains all the widgets"
        self.frm2 = tk.Frame(self.root,height= 100, width =100)
        self.frm2.grid(column=0, row =0)
        self.btn = tk.Button(self.frm2,text="Open a folder",command=lambda:self.listbox(),width = 15)
        self.btn.grid(column=0,row=0,sticky = 'nsew')
        self.btn3 = tk.Button(self.frm2,text="Quit",command=lambda:self.Exit(),width = 15)
        self.btn3.grid(column=0,row=2,sticky = 'nsew')
        self.btn2 = tk.Button(self.frm2,text="HOL to LFD",command = lambda:self.hol2lfd_input_page(self.frm2),width = 15)
        self.btn2.grid(column = 0, row = 1,sticky = 'nsew')

        self.auto_resizer(self.frm2,1)


            
    def frame1(self):
        "Contains the list of filenames in listbox"
        self.frm1 = tk.Frame(self.root)
        self.frm1.grid(column=1, row=0, sticky="nswe")
        
    def frame3(self):
        "Contains  the Preview"
        self.frm3 = tk.Frame(self.root)
        self.frm3.grid(column=2, row=0)
        
    def frame4(self):
        "Contains the slider"
        self.frm4 = tk.Frame(self.root)
        self.frm4.grid(column=3,row=0)
    def frame5(self):
        "Contains the row/column placeholder"
        self.frm5 = tk.Frame(self.root)
        self.frm5.grid(column=2, row=1)

    def listbox(self):
        self.widgets_order()
        "Initial for excel file"
        self.position = 1
        ws.write_row(0, 0, ["filename"])
        ws.write_row(1, 0, ["Image quality"])
        "the listbox in the frame1"
        self.lbx = tk.Listbox(self.frm1, bg="white")
        self.lbx.grid(column=1, row=0,sticky="nswe")
        #self.lbx.pack(side="left", fill=tk.Y, expand=1)
        "Initial the buttons"
        self.btn2 = tk.Button(self.frm2,text="Next",command=lambda:self.next_selection(),width = 15)
        self.btn2.grid(column=0,row=2)
        self.btn4 = tk.Button(self.frm2,text="Display",command=lambda:self.showimage(),width = 15)
        self.btn4.grid(column=0,row=1)
        self.btn = tk.Button(self.frm2,text="Open a new folder",command=lambda:self.additems(),width = 15)
        self.btn.grid(column=0,row=0)
        self.folder = self.additems()

    def next_selection(self):
        #current selection
        selection_indices = self.lbx.curselection()
        # default next selection is the beginning
        next_selection = 0

        # make sure at least one item is selected
        if len(selection_indices) > 0:
            # Get the last selection, remember they are strings for some reason
            # so convert to int
            last_selection = int(selection_indices[-1])

            # clear current selections
            self.lbx.selection_clear(selection_indices)

            # Make sure we're not at the last item
            if last_selection < self.lbx.size() - 1:
                next_selection = last_selection + 1
        #preview the next selection
        self.lbx.activate(next_selection)
        self.lbx.selection_set(next_selection)
        imgname = self.lbx.get(self.lbx.curselection())
        image = Image.open(self.folder+"/"+imgname)
        resize_image = image.resize((600,600))
        img = ImageTk.PhotoImage(resize_image)
        self.image = img
        self.lab["image"] = img

    def showimage(self):
        #get current selection
        imgname = self.lbx.get(self.lbx.curselection())
        #manipulation required for numpy array conversion
        img = Image.open(self.folder+"\\"+imgname)
        #numpy array conversion
        numpydata = asarray(img)
        #flip the view and convert the color to RGB
        numpydata=numpydata.view()[::-1, :, :]
        numpydata=cv2.cvtColor(numpydata, cv2.COLOR_BGR2RGB)
        numpyshape=numpydata.shape
        #ask for manual or auto number of row/column
        answer = askyesno(title='Manual Row,Column', message='Do you want to Manually insert nb of row and column?')
        #show placeholder to insert nb of row/column
        if answer:
            self.lblentry1 = tk.Label(self.frm5,text="vx :")
            self.lblentry1.grid(row=1,column=0,sticky="nswe")
            self.entry1 = tk.Entry(self.frm5)
            self.entry1.grid(row=1, column=1,sticky="nswe")
            self.lblentry2 = tk.Label(self.frm5,text="vy :")
            self.lblentry2.grid(row=1,column=2,sticky="nswe")
            self.entry2 = tk.Entry(self.frm5)
            self.entry2.grid(row=1, column=3,sticky="nswe")
            self.btnv = tk.Button(self.frm5,text="Valid",command=lambda:self.send())
            self.btnv.grid(row=1, column=4)
        else:
            #take the first and the second number in the file name
            vx = int(re.findall("\d+",imgname)[0])
            vy = int(re.findall("\d+",imgname)[1])
            #create the correct format for the image to be send
            if vx and vy != None:
                numpydata={'cmd': {'show': {'targetDisplay': 0, 'source': 'bindata', 'quilt': {'type': 'image', 'settings': {'vx': vx, 'vy': vy, 'vtotal': vx*vy, 'aspect': 0.75, 'invert': False}}}}, 'bin': numpydata}
                print('Loading Image')
                #send the image to the LGP
                cbordump = cbor.dumps(numpydata,image_shape=numpyshape)
                self.__socket.send(cbordump)
            #evaluate the image and save it to the excel file
            self.btn5 = tk.Button(self.frm2, text="Close image",command=lambda:self.Hide(),width = 15)
            self.btn5.grid(column=0,row=3)
            self.sldr = tk.Scale(self.frm4, from_=1, to=100)
            self.sldr.grid(column=3,row=0)
            e1 = simpledialog.askinteger(title="Image Compression Quality",
                        prompt="Rate between 1-100?:",minvalue=1, maxvalue=100)
            ws.write_row(0, self.position, [self.lbx.get(self.lbx.curselection())])
            ws.write(1,self.position,e1)
            self.position +=1

    def send(self):
        imgname = self.lbx.get(self.lbx.curselection())

        img = Image.open(self.folder+"/"+imgname)

        numpydata = asarray(img)

        numpydata=numpydata.view()[::-1, :, :]
        numpydata=cv2.cvtColor(numpydata, cv2.COLOR_BGR2RGB)
        numpyshape=numpydata.shape

        vx=int(self.entry1.get())
        vy=int(self.entry2.get())
        #create the correct format for the image to be send
        if vx and vy != None:
            numpydata={'cmd': {'show': {'targetDisplay': 0, 'source': 'bindata', 'quilt': {'type': 'image', 'settings': {'vx': vx, 'vy': vy, 'vtotal': vx*vy, 'aspect': 0.75, 'invert': False}}}}, 'bin': numpydata}
            print('Loading Image')
            #send the image to the LGP
            cbordump = cbor.dumps(numpydata,image_shape=numpyshape)
            self.__socket.send(cbordump)
        #evaluate the image and save it to the excel file
        self.btn5 = tk.Button(self.frm2, text="Close image",command=lambda:self.Hide(),width = 15)
        self.btn5.grid(column=0,row=3)
        self.sldr = tk.Scale(self.frm4, from_=1, to=100)
        self.sldr.grid(column=3,row=0)
        e1 = simpledialog.askinteger(title="Image Compression Quality",
                prompt="Rate between 1-100?:",minvalue=1, maxvalue=100)
        ws.write_row(0, self.position, [self.lbx.get(self.lbx.curselection())])
        ws.write(1,self.position,e1)
        self.position +=1

    def Hide(self):
        #close the image on the LGP
        hide = cbor.dumps({'cmd': {'hide': {'targetDisplay': 0}}, 'bin': b''})
        self.__socket.send(hide)

    def additems(self):
        #ask for the folder containing the images
        self.lbx.delete(0,END)
        self.folder = tk.filedialog.askdirectory(initialdir="C:") #modify the path

        files = os.listdir(self.folder)
        answer = askyesno(title='List', message='Do you want to randomize the list?')
        #randomize the list
        if answer:
            random.shuffle(files)
        for file in files:
            if file.endswith(".png"):
                self.lbx.insert("anchor",file)
        #preview the first item of the list
        self.lbx.select_set(0)
        imgname = self.lbx.get(self.lbx.curselection())
        image = Image.open(self.folder+"/"+imgname)
        resize_image = image.resize((600,600))
        img = ImageTk.PhotoImage(resize_image)
        self.image = img
        self.lab["image"] = img
        return self.folder

    def label(self):
        self.lab = tk.Label(self.frm3)
        self.lab.grid(column=2,row=0)

    def Exit(self):
        #close the GUI
        hide = cbor.dumps({'cmd': {'hide': {'targetDisplay': 0}}, 'bin': b''})
        self.__socket.send(hide)
        wb.close()
        exit()

    def widgets_order(self):
        self.frame1()
        self.frame3()
        self.label()
        self.frame4()
        self.frame5()
#######################################################################################################

class TextRedirector:
    def __init__(self, widget):
        self.widget = widget

    def write(self, string):
        try:
            if self.widget:
                self.widget.config(state=tk.NORMAL)
                self.widget.insert(tk.END, string)
                self.widget.see(tk.END)
                self.widget.config(state=tk.DISABLED)
        except:
            print('Text Redirector error')
    def flush(self):
        pass  # If you want something done when flush is called, put it here


win = Win()
win.root.mainloop()  
