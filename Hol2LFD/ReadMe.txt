############################ READ ME FOR HOL2LFD #################################

dirs ---------------------------------------

~/6x8_figures/ == preconstructed 6x8 all figures.
~/11x11_specular_car/ == preconstructed 11x11 specular car.
~/hologram_images/ == holographic images downloaded from BCOM database. https://hologram-repository.labs.b-com.com/#/holographic-images
~/jpeg_pleno_nrsh/ == NRSH software. https://gitlab.com/wg1/jpeg-pleno-nrsh
~/reconstruction_configs/ == preset configs for reconstruction 
~/cbor/ == necessary library for writing images to LFD. Add this to your python PATH or copy this folder to python scripts 
~/test/ == some test images 

GUI.py
hol2lfd.m
image_functions.py


Set Up -------------------------------------

* Download MATLAB R2023a and setup. Required for python API of MATLAB. https://www.mathworks.com/products/new_products/latest_features.html
* Download and setup python 3.10. https://www.python.org/downloads/release/python-3100/
* Download and setup Looking Glass Bridge and Looking Glass Studio (only if you want to connect to LGP) https://lookingglassfactory.com/software-downloads

***** for LGP simulation you can try this library but it has not been tested nor implemented https://github.com/jankais3r/VirtualHPS

* Install below libraries for python:

# library and module
import random
import tkinter as tk
import os, re
from tkinter import END, filedialog
from tkinter import simpledialog
from tkinter.messagebox import askyesno
from PIL import Image, ImageTk
import pynng, cv2
from numpy import asarray
from cbor import cbor
import xlsxwriter
import sys
import matlab.engine
import time
import itertools
import numpy as np
from PIL import Image

~/cbor/ == necessary library for writing images to LFD. Add this to your python PATH or copy this folder to python scripts 

Run ----------------------------------------

 * Run the program by running GUI.py 

 * image_functions.py contains last stage where the rendered images are checked and quilt image is constructed. (This is called inside GUI.py, no extra steps)

 * hol2lfd.m contains matlab side functionality of the program called inside python

 * quilt images are saved directly to this folder.
