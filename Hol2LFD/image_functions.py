from PIL import Image
import os
import glob
import numpy as np

class Hol2LFD:

    def __init__(self, im_location,name_prefix,hangle, vangle,aperture_size, rec_dist, no_rows, no_columns):
        self.im_location = im_location
        self.name_prefix = name_prefix
        self.im_name = name_prefix
        self.hangle = hangle
        self.vangle = vangle
        self.no_rows = no_rows
        self.no_columns = no_columns
        self.output_name = ''
        self.aperture_size = aperture_size
        self.rec_dist = rec_dist


        self.images = {}
        self.missing_files = []


    # Function to check if the images exists
    def check_images(self):
        total_pictures = self.no_rows*self.no_columns
        for i in range(1,len(self.hangle)+1):
            # Or, use divmod to get both at once
            row, column = divmod(i-1, self.no_columns)
            if all(x == self.vangle[0] for x in self.vangle):
                self.filename = f"{self.im_location}/{self.name_prefix}_{self.hangle[-i]}_{self.vangle[0]}_{self.aperture_size}_{self.rec_dist}_LR.png"
            else:
                self.filename = f"{self.im_location}/{self.name_prefix}_{self.hangle[-i]}_{self.vangle[-(row+1)]}_{self.aperture_size}_{self.rec_dist}_LR.png"
            print(len(self.hangle),i)

            # Check if the file exists
            if os.path.exists(self.filename):
                # Save the filename in the dictionary
                self.images[f"{row}_{column}_{self.aperture_size}_{self.rec_dist}"] = self.filename
                print(self.images)
            else:
                print(f"Image file does not exist: {self.filename}")
                self.missing_files.append(self.filename)
            # If there are no missing files, open them and replace the filename with the image data
        if not self.missing_files:
            return self.images
        else:
            print(self.images)
            return -1

    def check_images_init(self):
        for i in range(1,self.no_rows+1):
            for j in range(1,self.no_columns+1):
        # Create the filename
                self.filename = f"{self.im_location}/{self.im_name}_{self.hangle[-i]}_{self.vangle[-j]}_{self.aperture_size}_{self.rec_dist}_LR.png"
                # Check if the file exists
                if os.path.exists(self.filename):
                    # Save the filename in the dictionary
                    self.images[f"{i-1}_{j-1}_{self.aperture_size}_{self.rec_dist}"] = self.filename
                else:
                    print(f"Image file does not exist: {self.filename}")
                    self.missing_files.append(self.filename)

        # If there are no missing files, open them and replace the filename with the image data
        if not self.missing_files:
            return self.images
        else:
            return -1

    def concatenate_images(self):
        print(self.images)
        init_flag = 0
        total_width = 8 * 1024  # 4*1024
        total_height = 6 * 1024  # 4*1024
        # Create a new image of the right size
        new_img = Image.new('RGB', (total_width, total_height))
        init_flag = 1
        # Assuming all images are the same size, get the dimensions of the first image
        for key, filename in self.images.items():
            try:
                with Image.open(filename) as img:
                    img_resized = img.resize((total_width//self.no_columns, total_height//self.no_rows), Image.BILINEAR)
                    width, height = img_resized.size
                    # Parse the row and column from the key
                    row,col, aper,rec_d = key.split('_')
                    row = int(row)
                    col = int(col)
                    # Calculate the position to paste this image
                    x = col * width
                    y = row * height
                    # Paste this image
                    new_img.paste(img_resized, (x, y))
            except IOError:
                print(f"Error opening image file: {filename}")
        try:
            if new_img is not None:
                new_img.save(f"{self.im_name}_to_LFD.png")
                return new_img
            else:
                return None
        except Exception as e:
            print(f"Failed to save image {self.im_name} : {e}")
